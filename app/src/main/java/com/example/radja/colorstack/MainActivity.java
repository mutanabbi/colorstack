package com.example.radja.colorstack;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

class PersistentInfo implements Serializable {
    public PersistentInfo(String txt, int txt_c, int bg_c) {
        text = txt;
        text_color = txt_c;
        bg_color = bg_c;
    }
    public final String text;
    public final int text_color;
    public final int bg_color;
}

public class MainActivity extends AppCompatActivity {

    private ViewGroup m_main_layout;
    private ViewGroup m_output_layout;
    private EditText m_main_input_field;
    private ArrayList<PersistentInfo> m_list_of_rows = new ArrayList<>();

    private static int getContrastColor(int color) {
        final double y = (299 * Color.red(color) + 587 * Color.green(color) + 114 * Color.blue(color)) / 1000;
        return y >= 128 ? Color.BLACK : Color.WHITE;
    }

    private void create_sub_layout(String text, int text_color, int bg_color) {
        final LayoutInflater inflater = LayoutInflater.from(this);
        final View sub_layout = inflater.inflate(R.layout.sub_layout, null);
        final TextView sub_tv = (TextView) sub_layout.findViewById(R.id.out_text);
        sub_tv.setBackgroundColor(bg_color);
        sub_tv.setTextColor(text_color);
        sub_tv.setText(text);
        final Button sub_btn = (Button) sub_layout.findViewById(R.id.remove_btn);
        sub_btn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                final View sub_layout = (View) view.getParent();
                final int idx = m_output_layout.indexOfChild(sub_layout);
                m_list_of_rows.remove(idx);
                m_output_layout.removeView(sub_layout);
            }
        });
        m_output_layout.addView(sub_layout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_main_layout = (ViewGroup) findViewById(R.id.main_layout);
        m_output_layout = (ViewGroup) m_main_layout.findViewById(R.id.output_layout);
        m_main_input_field = (EditText) findViewById(R.id.main_input_field);
        final Button add_btn = (Button) findViewById(R.id.add_btn);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                if (m_main_input_field.getText().toString().trim().equals(""))
                    return;
                final Random rnd = new Random();
                final int bg_color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                final String text = m_main_input_field.getText().toString();
                final int text_color = getContrastColor(bg_color);
                m_list_of_rows.add(new PersistentInfo(text, text_color, bg_color));
                m_main_input_field.getText().clear();
                create_sub_layout(text, text_color, bg_color);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        state.putSerializable("list_of_rows", m_list_of_rows);
        super.onSaveInstanceState(state);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (state.containsKey("list_of_rows")) {
            m_list_of_rows = (ArrayList<PersistentInfo>) state.getSerializable("list_of_rows");
            for (final PersistentInfo info : m_list_of_rows) {
              create_sub_layout(info.text, info.text_color, info.bg_color);
            }
        }
    }
}
